//
//  AppDelegate.swift
//  Hiddy
//
//  Created by APPLE on 29/05/18.
//  Copyright © 2018 HITASOFT. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import PushKit
import SANotificationViews
import CallKit
import GSImageViewerController
import AVFoundation
import WebRTC
import FirebaseUI
import Firebase
import FirebaseMessaging
let localNotify = RNNotificationView()


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,PKPushRegistryDelegate,CXProviderDelegate,updateDelegate {
    var window: UIWindow?
    var badgeNo:Int = 0
    var getData = Bool()
    var callStarted = Bool()
    var baseUUId = UUID()
    var callStatus : String!
    var localCallDB = CallStorage()
    var callNotificationDict = NSMutableDictionary()
    //inital data set up
    var contactArray = NSMutableArray()
    var groupArray = NSMutableArray()
    var ownChannelArray = NSMutableArray()
    var allChannelArray = NSMutableArray()
    var callsArray = NSMutableArray()
    var callController = CXCallController()
    var provider = CXProvider(configuration: CXProviderConfiguration(localizedName: "Hiddy"))
    var callKitPopup = false
    var Voip_apns_Status = 0
    var currentCallerID = String()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        // Override point for customization after application launch.
        //        let dict = NSMutableDictionary()
        //        dict.setValue("ddd", forKey: "dd")
        //        UserModel.shared.setCallDict(callDict: dict)
        FirebaseApp.configure()
        UserModel.shared.setCallSocket(status: nil)
        // Fetch data once an hour.
        UIApplication.shared.setMinimumBackgroundFetchInterval(1)
        Thread.sleep(forTimeInterval: 3.0)
        if (launchOptions?[UIApplication.LaunchOptionsKey.url] as? URL) != nil {
            // TODO: handle URL from here
        }
        else {
            
        }
        
        self.initialSetup()
        self.checkUserLoggedStatus()
        self.getData = false
        //config push notification
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        //Added Code to display notification when app is in Foreground
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
        
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        voipRegistry.desiredPushTypes = Set([PKPushType.voIP])
        voipRegistry.delegate = self;
        self.createDocument()
        // Config firebase
        if let notification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: AnyObject] {
           print("notificationReceived\(notification)")
        }
        return true
    }
    func createDocument() {
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let logsPath = documentsDirectoryURL.appendingPathComponent(DOCUMENT_PATH)
        do
        {
            try FileManager.default.createDirectory(atPath: logsPath.path, withIntermediateDirectories: true, attributes: nil)
        }
        catch let error as NSError
        {
            NSLog("Unable to create directory \(error.debugDescription)")
        }
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        let localObj = LocalStorage()
        let groupObj = groupStorage()
        let channelObj = ChannelStorage()
        
        let badgeNumber = localObj.overAllUnreadMsg() + groupObj.groupOverAllUnreadMsg() + channelObj.channelOverAllUnreadMsg()
        UIApplication.shared.applicationIconBadgeNumber = badgeNumber
        self.badgeNo = 0
        //        socketClass.sharedInstance.disconnect()
        // Print("*******RESIGN ACTIVE********")
        
    }
    // [START receive_message] remote notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any])  {
        print(userInfo)
        // This notification is not auth related, developer should handle it.
        if(UserModel.shared.userID() != nil) {
            badgeNo = badgeNo + 1
            //get msg from notification
            let strVal = userInfo["message_data"] as! NSString
            print("notification: \(strVal)")
            
            let msgDictVal = convertToDictionary(text: "\(strVal)")!
            let msgDict: NSDictionary = msgDictVal as NSDictionary
            let chat_type:String = msgDict.value(forKey: "chat_type") as! String
            if chat_type == "group" {
                self.groupMsg(msgDict: msgDict)
            }else if chat_type == "single"{
                self.privateMsg(msgDict: msgDict)
            }else if chat_type == "channel"{
                self.channelMsg(msgDict: msgDict)
            }else if chat_type == "groupinvitation"{
//                self.loadAllUnreadMessages()
            }else if chat_type == "channelinvitation"{
//                self.triggerNotification(msgDict: msgDict, userDict: msgDict, type:"channelinvitation")
//                self.loadAllUnreadMessages()
            }
            
            let localObj = LocalStorage()
            let groupObj = groupStorage()
            let channelObj = ChannelStorage()
            
            let badgeNumber = localObj.overAllUnreadMsg() + groupObj.groupOverAllUnreadMsg() + channelObj.channelOverAllUnreadMsg()
            UIApplication.shared.applicationIconBadgeNumber = badgeNumber
        }
        
        //        Auth.auth().canHandleNotification(userInfo)
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    private func scheduleLocalNotification() {
        // Create Notification Content
        let notificationContent = UNMutableNotificationContent()

        // Configure Notification Content
        notificationContent.title = "Cocoacasts"
        notificationContent.subtitle = "Local Notifications"
        notificationContent.body = "In this tutorial, you learn how to schedule local notifications with the User Notifications framework."

        // Add Trigger
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 10.0, repeats: false)

        // Create Notification Request
        let notificationRequest = UNNotificationRequest(identifier: "cocoacasts_local_notification", content: notificationContent, trigger: notificationTrigger)

        // Add Request to User Notification Center
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            if let error = error {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
            }
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        if Auth.auth().canHandleNotification(userInfo) {
            completionHandler(.noData)
            return
        }
        // This notification is not auth related, developer should handle it.
        
        if(UserModel.shared.userID() != nil) {
            badgeNo = badgeNo + 1
            //get msg from notification
            let msgDict = userInfo["message_data"] as! NSDictionary
            print("notification: \(msgDict)")
            
//            let msgDictVal = convertToDictionary(text: "\(strVal)")!
//            let msgDict: NSDictionary = msgDictVal as NSDictionary
            let chat_type:String = msgDict.value(forKey: "chat_type") as! String
            if chat_type == "group" {
                self.groupMsg(msgDict: msgDict)
            }else if chat_type == "single"{
                self.privateMsg(msgDict: msgDict)
            }else if chat_type == "channel"{
                self.channelMsg(msgDict: msgDict)
            }else if chat_type == "groupinvitation"{
//                self.loadAllUnreadMessages()
            }else if chat_type == "channelinvitation"{
//                self.triggerNotification(msgDict: msgDict, userDict: msgDict, type:"channelinvitation")
//                self.loadAllUnreadMessages()
            }
            
            let localObj = LocalStorage()
            let groupObj = groupStorage()
            let channelObj = ChannelStorage()
            
            let badgeNumber = localObj.overAllUnreadMsg() + groupObj.groupOverAllUnreadMsg() + channelObj.channelOverAllUnreadMsg()
            UIApplication.shared.applicationIconBadgeNumber = badgeNumber
        }
        //        Auth.auth().canHandleNotification(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
        UserModel.shared.setAPNSToken(fcm_token: deviceTokenString as NSString)
        self.callPushNotification()
        Auth.auth().setAPNSToken(deviceToken, type: .prod)
        Messaging.messaging().setAPNSToken(deviceToken, type: .prod)
        Messaging.messaging().delegate = self
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //connect socket
        //        if !callStarted {
        //            socketClass.sharedInstance.disconnect()
        //        }
        // Print("*******BACKGROUND********")
        self.getData = false
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        let localObj = LocalStorage()
        UIApplication.shared.applicationIconBadgeNumber = localObj.overAllUnreadMsg()
        self.badgeNo = 0
        //connect socket
        
        if(UserModel.shared.userID() != nil) {
            if !callStarted{
                socketClass.sharedInstance.connect()
            }
            if !self.getData{
                socketClass.sharedInstance.getRecentMsg()
                groupSocket.sharedInstance.getNewGroup()
                channelSocket.sharedInstance.getNewChannel()
                DispatchQueue.global(qos: .background).async {
                    Contact.sharedInstance.synchronize()
                }
                if UserModel.shared.isRegistered() == "1"{
                    Utility.shared.checkDeviceInfo()
                }
                self.getData = true
                
            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: initial setup
    func initialSetup(){
        
        UserModel.shared.setSocket(status:"0")
        //setup language
        if UserModel.shared.getAppLanguage() == nil{
            UserModel.shared.LANGUAGE_CODE = "en"
            UserModel.shared.setAppLanguage(Language: DEFAULT_LANGUAGE)
        }else{
            UserModel.shared.setAppLanguage(Language: UserModel.shared.getAppLanguage()!)
        }
        Utility.shared.configureLanguage()
        
        //keyborad manager
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        self.configWebRTC()
        microPhonePermission()
    }
    
    
    //initialise webrtc
    func configWebRTC()  {
        let fieldTrials: [AnyHashable : Any] = [:]
        RTCInitFieldTrialDictionary(fieldTrials as? [String : String])
        RTCInitializeSSL()
        RTCSetupInternalTracer()
        #if NDEBUG
        // In debug builds the default level is LS_INFO and in non-debug builds it is
        // disabled. Continue to log to console in non-debug builds, but only
        // warnings and errors.
        RTCSetMinDebugLogLevel(RTCLoggingSeverityWarning)
        #endif
    }    //MARK: check user status
    func checkUserLoggedStatus()  {
        if(UserModel.shared.userID() == nil) {
            self.setInitialViewController(initialView: LoginPage())
        }else{
            self.setInitialViewController(initialView: menuContainerPage())
        }
        if Utility.shared.isConnectedToNetwork(){
            
            let userObj = UserWebService()
            userObj.versionUpdate(onSuccess: {response in
                let status:Int = response.value(forKey: "status") as! Int
                if status == 1{
                    let currentVersion:String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
                    let newVersion:String = response.value(forKey: "ios_version") as! String
                    
                    
                    if newVersion.compare(currentVersion, options: .numeric) == .orderedDescending {
                        // Print("store version is newer")
                        let forceUpdate:NSNumber = response.value(forKey: "ios_update") as! NSNumber
                        let alert = AppUpdate()
                        alert.delegate = self
                        alert.viewType = "\(forceUpdate)"
                        DispatchQueue.main.async {
                            self.setInitialViewController(initialView: alert)
                        }
                    }
                    
                }
            })
        }
    }
    
    func updateDismiss(){
        if(UserModel.shared.userID() == nil) {
            self.setInitialViewController(initialView: LoginPage())
        }else{
            self.setInitialViewController(initialView: menuContainerPage())
        }
    }
    // MARK:set initial view controller
    func setInitialViewController(initialView: UIViewController)  {
        
        UserModel.shared.setTab(index: 0)
        window = UIWindow(frame: UIScreen.main.bounds)
        let homeViewController = initialView
        let nav = UINavigationController(rootViewController: homeViewController)
        nav.isNavigationBarHidden = true
        window!.rootViewController = nav
        window!.makeKeyAndVisible()
    }
    
    func setAfterStatusViewController(initialView: UIViewController)  {
        UserModel.shared.setTab(index: 0)
        window = UIWindow(frame: UIScreen.main.bounds)
        let homeViewController = initialView
        let nav = UINavigationController(rootViewController: homeViewController)
        nav.isNavigationBarHidden = true
        window!.rootViewController = nav
        //        window!.makeKeyAndVisible()
    }
    func PushKitRegistration(){
        let mainQueue = DispatchQueue.main
        // Create a push registry object
        if #available(iOS 8.0, *) {
            let voipRegistry: PKPushRegistry = PKPushRegistry(queue: mainQueue)
            // Set the registry's delegate to self
            voipRegistry.delegate = self
            // Set the push type to VoIP
            voipRegistry.desiredPushTypes = [PKPushType.voIP]
        }else {
            // Fallback on earlier versions
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        // Print("pushRegistry didInvalidatePushTokenFor \(type)")
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        
        let deviceTokenString = pushCredentials.token.reduce("", {$0 + String(format: "%02X", $1)})
        //         print("PUSH KIT TOKEN \(deviceTokenString)")
        UserModel.shared.setPushToken(fcm_token: deviceTokenString as NSString)
        
        self.callPushNotification()
        
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        // Print("PUSHKIT NOTIFICATION \(payload.dictionaryPayload)")
        if(UserModel.shared.userID() != nil) {
            badgeNo = badgeNo + 1
            //get msg from notification
            let msgDict:NSDictionary = payload.dictionaryPayload["message_data"] as! NSDictionary
            let chat_type:String = msgDict.value(forKey: "chat_type") as! String
            if chat_type == "call"{
                self.callMsg(msgDict: msgDict)
            }
        }
    }
    //generate notification
    func triggerNotification(msgDict:NSDictionary,userDict:NSDictionary,type:String)  {
        let content = UNMutableNotificationContent()
        var identifier = String()
        let cryptLib = CryptLib()
        
        if type == "single"{
            let localObj = LocalStorage()
            let contact_id:String = msgDict.value(forKey: "sender_id") as? String ?? ""
            let unreadcount = localObj.getUnreadCount(contact_id: contact_id)
            let body = msgDict.value(forKey: "message") as? String ?? ""
            let decryptedMsg = cryptLib.decryptCipherTextRandomIV(withCipherText: body, key: ENCRYPT_KEY)
            
            if userDict.value(forKey: "contact_name") != nil{
                content.title = userDict.value(forKey: "contact_name") as! String
            }else{
                content.title = userDict.value(forKey: "contact_no") as? String ?? ""
            }
            if unreadcount == 0{
                content.body = decryptedMsg ?? ""
            }else{
                //                content.body = "\(unreadcount) \((msgDict.value(forKey: "message"))!)"
                content.body = decryptedMsg ?? ""
            }
            identifier = msgDict.value(forKey: "message_id") as! String
            content.threadIdentifier = content.title
            content.summaryArgument = content.title
            content.summaryArgumentCount = 2
            content.categoryIdentifier = "single:\(contact_id)"
        }else if type == "group"{
            let groupObj = groupStorage()
            let group_id = msgDict.value(forKey: "group_id") as! String
            let unreadcount = groupObj.getGroupUnreadCount(group_id: group_id)
            content.title = userDict.value(forKey: "group_name") as? String ?? ""
            let member_id = msgDict.value(forKey: "member_id") as! String
            let body = msgDict.value(forKey: "message") as? String ?? ""
            let decryptedMsg = cryptLib.decryptCipherTextRandomIV(withCipherText: body, key: ENCRYPT_KEY)
            
            if unreadcount == 0{
                content.body = "\(Utility.shared.getUsername(user_id: member_id)) : \(decryptedMsg!)"
            }else{
                //                content.body = "\(unreadcount) \(Utility.shared.getUsername(user_id: member_id)) : \(decryptedMsg!)"
                content.body = "\(Utility.shared.getUsername(user_id: member_id)) : \(decryptedMsg!)"
            }
            identifier = msgDict.value(forKey: "message_id") as! String
            content.threadIdentifier = content.title
            content.summaryArgument = content.title
            content.categoryIdentifier = "group:\(group_id)"
        }else if type == "channel"{
            let channelObj = ChannelStorage()
            let channel_id:String = userDict.value(forKey: "channel_id") as! String
            let unreadcount = channelObj.getChannelUnreadCount(channel_id: channel_id)
            content.title = "Channel: \(userDict.value(forKey: "channel_name") as! String)"
            
            let body = msgDict.value(forKey: "message") as? String ?? ""
            let decryptedMsg = cryptLib.decryptCipherTextRandomIV(withCipherText: body, key: ENCRYPT_KEY)
            
            if unreadcount == 0{
                content.body = "\(decryptedMsg!)"
            }else{
                content.body = "\(decryptedMsg!)"
                //                content.body = "\(unreadcount) \((msgDict.value(forKey: "message"))!)"
            }
            identifier = msgDict.value(forKey: "message_id") as! String
            content.threadIdentifier = content.title
            content.summaryArgument = content.title
            content.categoryIdentifier = "channel:\(channel_id)"
        }else if type == "groupinvitation"{
            content.body = "You added to this group"
            content.title = userDict.value(forKey: "group_name") as? String ?? ""
            identifier = msgDict.value(forKey: "group_id") as! String
            content.categoryIdentifier = "groupinvitation:\(identifier)"
        }else if type == "channelinvitation"{
            content.body = "Channel Invitation"
            content.title = msgDict.value(forKey: "title") as! String
            identifier = msgDict.value(forKey: "id") as! String
            content.categoryIdentifier = "channelinvitation:\(identifier)"
        }
        content.sound = UNNotificationSound.default
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.1, repeats: false)
        let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.add(request) { (error) in
        }
    }
    //notification tap redirection
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // Print("TAPPED INFOR \(response)")
        socketClass.sharedInstance.connect()
        
        let category_type = response.notification.request.content.categoryIdentifier
        window = UIWindow(frame: UIScreen.main.bounds)
        let categoryArr = category_type.components(separatedBy: ":")
        UserModel.shared.setnotificationID(id:"1")
        
        if categoryArr[0] == "single"{
            UserModel.shared.setNotificationPrivateID(id:categoryArr[1])
            let chatObj = menuContainerPage()
            let nav = UINavigationController(rootViewController: chatObj)
            nav.isNavigationBarHidden = true
            window!.rootViewController = nav
            window!.makeKeyAndVisible()
            UserModel.shared.setTab(index: 0)
            
        }else if categoryArr[0] == "group"{
            UserModel.shared.setNotificationGroupID(id:categoryArr[1])
            let chatObj = menuContainerPage()
            let nav = UINavigationController(rootViewController: chatObj)
            nav.isNavigationBarHidden = true
            window!.rootViewController = nav
            window!.makeKeyAndVisible()
            UserModel.shared.setTab(index: 1)
            
        }else if categoryArr[0] == "channel"{
            UserModel.shared.setNotificationChannelID(id:categoryArr[1])
            let chatObj = menuContainerPage()
            let nav = UINavigationController(rootViewController: chatObj)
            nav.isNavigationBarHidden = true
            window!.rootViewController = nav
            window!.makeKeyAndVisible()
            UserModel.shared.setTab(index: 2)
        }else if categoryArr[0] == "groupinvitation"{
            let chatObj = menuContainerPage()
            let nav = UINavigationController(rootViewController: chatObj)
            nav.isNavigationBarHidden = true
            window!.rootViewController = nav
            window!.makeKeyAndVisible()
            UserModel.shared.setTab(index: 1)
            
        }else if categoryArr[0] == "channelinvitation"{
            let chatObj = menuContainerPage()
            let nav = UINavigationController(rootViewController: chatObj)
            nav.isNavigationBarHidden = true
            window!.rootViewController = nav
            window!.makeKeyAndVisible()
            UserModel.shared.setTab(index: 2)
        }
    }
    //microphone permission
    func microPhonePermission()  {
        AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
            
        })
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            
        }
        PhotoAlbum.init()//  create album
        
    }
    // one to one chat
    func privateMsg(msgDict:NSDictionary)  {
        let localObj = LocalStorage()
        let sender_id:String = msgDict.value(forKey: "sender_id") as! String
        let receiver_id:String = msgDict.value(forKey: "receiver_id") as! String
        
        // add to local & delivered status in background
        if UIApplication.shared.applicationState == .background || UIApplication.shared.applicationState == .inactive{
            let requestDict = NSMutableDictionary()
            requestDict.setValue(sender_id, forKey: "sender_id")
            requestDict.setValue(receiver_id, forKey: "receiver_id")
            requestDict.setValue(msgDict, forKey: "message_data")
            //add in local db
            Utility.shared.addToLocal(requestDict: requestDict, chat_id: "\(receiver_id)\(sender_id)", contact_id: sender_id)
            //delivered status api call
            let userService = UserWebService()
            userService.chatReceived(msgDict: msgDict, onSuccess: {response in
            })
            socketClass.sharedInstance.refreshView()
        }else if UIApplication.shared.applicationState == .active{
        }
        //check mute action
        
        let userDict = localObj.getContact(contact_id: sender_id)
        if (UserModel.shared.contactIDs()?.contains(sender_id))! {
            let mute:String = userDict.value(forKey: "mute") as! String
            if mute == "0" {
//                self.loadAllUnreadMessages()
                //                self.triggerNotification(msgDict: msgDict, userDict: userDict, type: "single")
            }
        }else{
            //other user need to register first
            let userObj = UserWebService()
            userObj.otherUserDetail(contact_id: sender_id, onSuccess: {response in
                let status:String = response.value(forKey: "status") as! String
                if status == STATUS_TRUE{
                    let phone_no :NSNumber = response.value(forKey: "phone_no") as! NSNumber
                    let cc = response.value(forKey: "country_code") as! Int
                    
                    localObj.addContact(userid: sender_id,
                                        contactName: ("+\(cc) " + "\(phone_no)"),
                                        userName: response.value(forKey: "user_name") as! String,
                                        phone: "\(phone_no)",
                        img: response.value(forKey: "user_image") as! String,
                        about: response.value(forKey: "about") as! String,
                        type: EMPTY_STRING,
                        mutual:response.value(forKey: "contactstatus") as! String,
                        privacy_lastseen: response.value(forKey: "privacy_last_seen") as! String,
                        privacy_about: response.value(forKey: "privacy_about") as! String,
                        privacy_picture: response.value(forKey: "privacy_profile_image") as! String, countryCode: String(cc))
//                    self.loadAllUnreadMessages()
                    //                    self.triggerNotification(msgDict: msgDict, userDict: userDict,type:"single")
                }
            })
        }
    }
    func loadAllUnreadMessages() {
        let localObj = LocalStorage()
        let recentArray = localObj.getRecentList(isFavourite: "0")
        let favArray = localObj.getRecentList(isFavourite: "1")
        recentArray.addObjects(from: favArray as [AnyObject])
        
        for i in recentArray {
            let recentDict:NSDictionary = i as! NSDictionary
            let contactID = recentDict.value(forKey: "sender_id") as? String ?? ""
            let recentCount = recentDict.value(forKey: "unread_count") as? String ?? ""
            let mute:String = recentDict.value(forKey: "mute") as! String
            
            if recentCount != "0" && mute == "0" {
                let recentArrayVal = localObj.getAllUnreadMsg(contact_id: contactID)
                let userDict = localObj.getContact(contact_id: contactID)
                for msg in recentArrayVal {
                    let msgDict:NSDictionary = msg as! NSDictionary
                    let msgType:String = msgDict.value(forKey: "message_type") as! String
                    if msgType == "isDelete" {
                        msgDict.setValue("This message was deleted", forKey: "message")
                    }
                    self.triggerNotification(msgDict: msgDict, userDict: userDict,type:"single")
                    
                }
            }
        }
        let groupObj = groupStorage()
        let groupArray = groupObj.getGroupList()
        for i in groupArray {
            let recentDict:NSDictionary = i as! NSDictionary
            let recentCount = recentDict.value(forKey: "unread_count") as? String ?? ""
            let groupID = recentDict.value(forKey: "group_id") as? String ?? ""
            let mute:String = recentDict.value(forKey: "mute") as! String
            if recentCount != "0" && mute == "0"{
                let recentArrayVal = groupObj.getGroupUnreadMessage(group_id: groupID)
                let groupDict = groupObj.getGroupInfo(group_id: groupID)
                
                for msg in recentArrayVal {
                    let msgDict:NSDictionary = msg as! NSDictionary
                    let groupType = msgDict.value(forKey: "message_type") as! String
                    if groupType == "isDelete" {
                        msgDict.setValue("This message was deleted", forKey: "message")
                    }
                    if groupType != "create_group" && groupType != "user_added" {
                        self.triggerNotification(msgDict: msgDict, userDict: groupDict,type:"group")
                    }
                    else {
                        self.triggerNotification(msgDict: msgDict, userDict: msgDict, type:"groupinvitation")
                    }
                }
            }
        }
        
        let channelObj = ChannelStorage()
        let ChannelArr = channelObj.getChannelNewList(type: "all")
        for i in ChannelArr {
            let recentDict:NSDictionary = i as! NSDictionary
            //            let contactID = recentDict.value(forKey: "admin_id") as? String ?? ""
            let recentCount = recentDict.value(forKey: "unread_count") as? String ?? ""
            let channel_id = recentDict.value(forKey: "channel_id") as? String ?? ""
            let mute:String = recentDict.value(forKey: "mute") as! String
            
            if recentCount != "0" && mute == "0"{
                let recentArrayVal = channelObj.getChannelUnreadMSg(channel_id: channel_id)
                let channelDict = channelObj.getChannelInfo(channel_id: channel_id)
                for msg in recentArrayVal {
                    let msgDict:NSDictionary = msg as! NSDictionary
                    let msgType:String = msgDict.value(forKey: "message_type") as! String
                    if msgType == "isDelete" {
                        msgDict.setValue("This message was deleted", forKey: "message")
                    }
                    self.triggerNotification(msgDict: msgDict, userDict: channelDict,type:"channel")
                    
                }
            }
        }
    }
    // one to one chat
    func groupMsg(msgDict:NSDictionary)  {
        
        // add to local & delivered status in background
        if UIApplication.shared.applicationState == .background || UIApplication.shared.applicationState == .inactive{
            //            let group_id:String = msgDict.value(forKey: "group_id") as! String
            //            groupSocket.sharedInstance.addGroupMsg(msgDict: msgDict, group_id: group_id)
            groupSocket.sharedInstance.checkAndAddGroupMsg(detailDict: msgDict)
            groupSocket.sharedInstance.refresh()
        }else if UIApplication.shared.applicationState == .active{
            groupSocket.sharedInstance.refresh()
        }
        
        let msgType:String = msgDict.value(forKey: "message_type") as! String
        if msgType != "add_member" && msgType != "left" && msgType != "group_image" && msgType != "remove_member" && msgType != "admin" && msgType != "subject"  && msgType != "change_number"{
            //check mute action
            let group_id:String = msgDict.value(forKey: "group_id") as! String
            let groupObj = groupStorage()
            if UserModel.shared.groupIDs().contains(group_id) {
                let groupDict = groupObj.getGroupInfo(group_id: group_id)
                let mute:String = groupDict.value(forKey: "mute") as! String
                if mute == "0" {
//                    loadAllUnreadMessages()
                    //                self.triggerNotification(msgDict: msgDict, userDict: groupDict, type: "group")
                }
            }else{
                //self.triggerNotification(msgDict: msgDict, userDict: groupDict, type: "group")
            }
            
        }
    }
    
    // channel msg
    func channelMsg(msgDict:NSDictionary)  {
        
        // add to local & delivered status in background
        if UIApplication.shared.applicationState == .background || UIApplication.shared.applicationState == .inactive{
            channelSocket.sharedInstance.refreshChannelMsg()
        }else if UIApplication.shared.applicationState == .active{
            channelSocket.sharedInstance.refreshChannelMsg()
        }
        
        let msgType:String = msgDict.value(forKey: "message_type") as! String
        if msgType != "added" && msgType != "subject" && msgType != "channel_image" && msgType != "channel_des"{
            //check mute action
            let channel_id:String = msgDict.value(forKey: "channel_id") as! String
            let channelObj = ChannelStorage()
            if UserModel.shared.channelIDs().contains(channel_id) {
                let channelDict = channelObj.getChannelInfo(channel_id: channel_id)
                let mute:String = channelDict.value(forKey: "mute") as! String
                if mute == "0" {
//                    loadAllUnreadMessages()
                    //                self.triggerNotification(msgDict: msgDict, userDict: channelDict, type: "channel")
                }
            }else{
                //self.triggerNotification(msgDict: msgDict, userDict: groupDict, type: "group")
            }
        }
    }
    
    func setInitialCache(){
        //        let recents = LocalStorage.sharedInstance.getRecentList()
        self.contactArray = LocalStorage.sharedInstance.getContactList()
        //        let groups = groupStorage.sharedInstance.getGroupList()
        //        self.groupArray =  UserDefaults.standard.object(forKey: "cache_groups") as! NSMutableArray
        // Print("gorup array count \(self.groupArray)")
        //        let all_channels = ChannelStorage.sharedInstance.getChannelList(type: "all")
        //        let own_channels = ChannelStorage.sharedInstance.getChannelList(type: "own")
    }
    
    
    func callMsg(msgDict:NSDictionary)    {
        self.callStarted = true
        socket.defaultSocket.connect()
        socket.defaultSocket.on(clientEvent: .connect){data, ack in
            socketClass.sharedInstance.connectChat()
            if UserModel.shared.callSocketStatus() == nil{
                callSocket.sharedInstance.CallSocketHandler()
                UserModel.shared.setCallSocket(status: "1")
            }
        }
        
        callNotificationDict.removeAllObjects()
        callNotificationDict.addEntries(from: msgDict as! [AnyHashable : Any])
        // init call kit for incoming call UI
        let call_status =  self.callNotificationDict.object(forKey: "call_type") as? String
        if call_status == "created"{
            if !callKitPopup{
                callStatus = "incoming"
                let receiver_id = self.callNotificationDict.object(forKey: "caller_id")as! String
                if (UserModel.shared.contactIDs()?.contains(receiver_id))! {
                    let localObj = LocalStorage()
                    let userDict = localObj.getContact(contact_id: receiver_id)
                    self.makeIncomingCall(userDict: userDict, receiver_id: receiver_id)
                }else{ // if user not availble
                    callFromUnknown(receiver_id: receiver_id)
                }
                self.currentCallerID = receiver_id
                self.sendPlatform()
                
            }else{//send another call option
                let receiver_id = self.callNotificationDict.object(forKey: "caller_id")as! String
                let call_id = self.callNotificationDict.object(forKey: "call_id")as! String
                let room_id = self.callNotificationDict.object(forKey: "room_id")as! String
                let type = self.callNotificationDict.object(forKey: "type") as! String
                let userid = UserModel.shared.userID()! as String
                callSocket.sharedInstance.createCall(callId: call_id, user_id:userid , caller_id: receiver_id, type: type,call_status: "outgoing", chat_type: "call",call_type:"waiting", room_id: room_id)
                
            }
        }else if call_status == "ended"{
            let receiver_id = self.callNotificationDict.object(forKey: "caller_id")as! String
            if self.currentCallerID == receiver_id{
                self.callKitPopup = false
                self.window?.rootViewController?.dismiss(animated: true, completion: nil)
                self.endCall()
            }
        }
    }
    func sendPlatform()  {
        let receiver_id = self.callNotificationDict.object(forKey: "caller_id")as! String
        let call_id = self.callNotificationDict.object(forKey: "call_id")as! String
        let room_id = self.callNotificationDict.object(forKey: "room_id")as! String
        let type = self.callNotificationDict.object(forKey: "type") as! String
        let userid = UserModel.shared.userID()! as String
        callSocket.sharedInstance.createCall(callId: call_id, user_id:userid , caller_id: receiver_id, type: type,call_status: "outgoing", chat_type: "call",call_type:"platform", room_id: room_id)
    }
    func providerDidReset(_ provider: CXProvider) {
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction)
    {
        // accepted call save in localDB    
        let time = NSDate().timeIntervalSince1970
        let receiver_id = self.callNotificationDict.object(forKey: "caller_id")as! String
        
        self.localCallDB.addNewCall(call_id: callNotificationDict.object(forKey: "call_id") as! String, contact_id: receiver_id, status: "incoming", call_type: callNotificationDict.object(forKey: "type")as! String, timestamp: "\(time.rounded().clean)", unread_count: "0")
        callStatus = "callAccept"
        let localObj = LocalStorage()
        let userDict = localObj.getContact(contact_id: receiver_id)
        self.perform(#selector(automaticDisconnect), with: nil, afterDelay: 28.0)
        // Print("status of socket  \(socket.status)")
        // accepted call navigate to voice call page
        if socket.status == .connected{
            //            sleep(3)
            let callSok = callSocket()
            callSok.acceptCall(callsender_id: callNotificationDict.object(forKey: "call_id") as! String, call_type: self.callNotificationDict.object(forKey: "type") as? String ?? "")
            let pageObj = CallPage()
            let platform = self.callNotificationDict.object(forKey: "platform") as! String
            let random_id = self.callNotificationDict.object(forKey: "call_id")
            pageObj.random_id = random_id as? String
            pageObj.receiverId = receiver_id
            pageObj.userdict = userDict
            pageObj.viewType = "2"
            pageObj.platform = platform
            pageObj.modalPresentationStyle = .overFullScreen
            pageObj.call_type = self.callNotificationDict.object(forKey: "type") as? String
            pageObj.room_id = self.callNotificationDict.object(forKey: "room_id") as? String
            pageObj.senderFlag = false
            self.window!.makeKeyAndVisible()
            self.window?.rootViewController?.present(pageObj, animated: true, completion: nil)
        }
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        callKitPopup = false
        action.fulfill()
        let receiver_id = self.callNotificationDict.object(forKey: "caller_id")as! String
        let call_id = "\(receiver_id)\(UserModel.shared.userID()!)"
        let room_id = self.callNotificationDict.object(forKey: "room_id")as! String
        let type = self.callNotificationDict.object(forKey: "type") as! String
        
        let userid = UserModel.shared.userID()! as String
        callSocket.sharedInstance.createCall(callId: call_id, user_id:userid , caller_id: receiver_id, type: type,call_status: "outgoing", chat_type: "call",call_type:"ended", room_id: room_id)
        
    }
    func endCallAct() {
        //        let time = NSDate().timeIntervalSince1970
        let receiver_id = self.callNotificationDict.object(forKey: "caller_id")as! String
        
        let localObj = LocalStorage()
        let userDict = localObj.getContact(contact_id: receiver_id)
        self.perform(#selector(automaticDisconnect), with: nil, afterDelay: 28.0)
        
        let pageObj = CallPage()
        let platform = self.callNotificationDict.object(forKey: "platform") as! String
        let random_id = self.callNotificationDict.object(forKey: "call_id")
        pageObj.random_id = random_id as? String
        pageObj.receiverId = receiver_id
        pageObj.userdict = userDict
        pageObj.viewType = "2"
        pageObj.platform = platform
        pageObj.modalPresentationStyle = .overFullScreen
        pageObj.call_type = self.callNotificationDict.object(forKey: "type") as? String
        pageObj.room_id = self.callNotificationDict.object(forKey: "room_id") as? String
        pageObj.senderFlag = false
        self.window!.makeKeyAndVisible()
        self.window?.rootViewController?.present(pageObj, animated: true, completion: nil)
        
    }
    @objc func automaticDisconnect()
    {
        if (callStatus == "incoming")
        {
            let endCallAction = CXEndCallAction(call:baseUUId)
            let transaction = CXTransaction(action: endCallAction)
            callController.request(transaction) { error in
                if let error = error {
                    // Print("EndCallAction transaction request failed: \(error.localizedDescription).")
                    //self.cxCallProvider.reportCall(with: call, endedAt: Date(), reason: .remoteEnded)
                    return
                }
                // Print("EndCallAction transaction request successful")
            }
            let time = NSDate().timeIntervalSince1970
            self.localCallDB.addNewCall(call_id: callNotificationDict.object(forKey: "call_id") as! String, contact_id: callNotificationDict.object(forKey: "caller_id")as! String, status: "missed", call_type: callNotificationDict.object(forKey: "type")as! String, timestamp: "\(time.rounded().clean)", unread_count: "1")
            callKitPopup = false
        }
        self.callStarted = false
    }
    
    func endCall(){
        // Print("end call uuid \(baseUUId)")
        let endCallAction = CXEndCallAction(call:baseUUId)
        let transaction = CXTransaction(action: endCallAction)
        callController.request(transaction) { error in
            if error != nil {
                // Print("EndCallAction transaction request failed: \(error.localizedDescription).")
                self.provider.reportCall(with: self.baseUUId, endedAt: Date(), reason: .remoteEnded)
                return
            }
            // Print("EndCallAction transaction request successful")
        }
        self.automaticDisconnect()
        self.callStarted = false
        callKitPopup = false
        //        self.endCallAct()
    }
    
    func setHomeAsRootView(){
        self.setInitialViewController(initialView: menuContainerPage())
    }
    //if user not availble
    func callFromUnknown(receiver_id:String)  {
        
        let userObj = UserWebService()
        userObj.otherUserDetail(contact_id: receiver_id, onSuccess: {response in
            let status:String = response.value(forKey: "status") as! String
            if status == STATUS_TRUE{
                let localObj = LocalStorage()
                let phone_no :NSNumber = response.value(forKey: "phone_no") as! NSNumber
                let cc = response.value(forKey: "country_code") as! Int
                
                localObj.addContact(userid: receiver_id,
                                    contactName: ("+\(cc) " + "\(phone_no)"),
                                    userName: response.value(forKey: "user_name") as! String,
                                    phone: "\(phone_no)",
                    img: response.value(forKey: "user_image") as! String,
                    about: response.value(forKey: "about") as! String,
                    type: EMPTY_STRING,
                    mutual:response.value(forKey: "contactstatus") as! String,
                    privacy_lastseen: response.value(forKey: "privacy_last_seen") as! String,
                    privacy_about: response.value(forKey: "privacy_about") as! String,
                    privacy_picture: response.value(forKey: "privacy_profile_image") as! String, countryCode: String(cc))
                let userDict = localObj.getContact(contact_id: receiver_id)
                self.makeIncomingCall(userDict: userDict, receiver_id: receiver_id)
            }
        })
    }
    
    
    func makeIncomingCall(userDict:NSDictionary,receiver_id:String)  {
        
        let session = AVAudioSession.sharedInstance()
        var availbleInput = NSArray()
        availbleInput = AVAudioSession.sharedInstance().availableInputs! as NSArray
        var port = AVAudioSessionPortDescription()
        port = availbleInput.object(at: 0) as! AVAudioSessionPortDescription
        var _: Error?
        try? session.setPreferredInput(port)
        try? session.setCategory(AVAudioSession.Category.playAndRecord, mode: .default, options: [])
        try? session.setMode(AVAudioSession.Mode.voiceChat)
        try? session.setMode(AVAudioSession.Mode.voiceChat)
        try? session.overrideOutputAudioPort(AVAudioSession.PortOverride.none)
        try? session.setActive(true)
        if UIApplication.shared.applicationState == .active{
            DispatchQueue.main.async {
                self.window?.rootViewController?.dismiss(animated: false, completion: nil)
                self.callStatus = "callAccept"
                let pageObj = CallPage()
                let random_id = self.callNotificationDict.object(forKey: "call_id")
                let platform = self.callNotificationDict.object(forKey: "platform") as! String
                pageObj.random_id = random_id as? String
                pageObj.receiverId = receiver_id
                pageObj.platform = platform
                pageObj.viewType = "1"
                pageObj.userdict = userDict
                pageObj.modalPresentationStyle = .overFullScreen
                pageObj.call_type = self.callNotificationDict.object(forKey: "type") as? String
                pageObj.room_id = self.callNotificationDict.object(forKey: "room_id") as? String
                
                pageObj.senderFlag = false
                
                //                let nav = UINavigationController(rootViewController: pageObj)
                //                nav.isNavigationBarHidden = true
                //                self.window!.rootViewController = nav
                //                self.window!.makeKeyAndVisible()
                
                self.window?.rootViewController?.present(pageObj, animated: true, completion: nil)
            }
        }else{
            baseUUId = UUID()
            provider.setDelegate(self, queue: nil)
            let update = CXCallUpdate()
            var username = String()
            if userDict.value(forKey: "contact_name") != nil{
                username = userDict.value(forKey: "contact_name") as! String
            }else{
                username = userDict.value(forKey: "user_phoneno") as! String
            }
            update.remoteHandle = CXHandle(type: .generic, value: username)
            if self.callNotificationDict.object(forKey: "type")as? String == "audio"{
                update.hasVideo = false
            }else{
                update.hasVideo = true
            }
            provider.reportNewIncomingCall(with: baseUUId, update: update, completion: { error in })
        }
        self.callKitPopup = true
    }
    
    
}
extension AppDelegate: forwardDelegate {
    func forwardMsg(type: String, msgDict: String) {
        self.window?.makeToast(Utility.shared.getLanguage()?.value(forKey: "sending") as? String)
    }
    
    
    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String?
        if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {
            return true
        }
        
        let homeVC = ForwardSelection()
        homeVC.msgFrom = "single"
        homeVC.delegate = self
        homeVC.shareTag = 1
        homeVC.modalPresentationStyle = .fullScreen
        let userDefaults = UserDefaults(suiteName: "group.com.hitasoft.Hiddy.Share")
        if let key = url.absoluteString.components(separatedBy: "=").last{
            if key == "image", let sharedArray = userDefaults?.object(forKey: "ImageSharePhotoKey") as? [Data]  {
                var imageArray: [CellModel] = []
                for i in 0..<sharedArray.count {
                    let urlArray = userDefaults?.object(forKey: "local_path") as? [String]
                    let model = CellModel(image: UIImage(data: sharedArray[i])!, imageData: sharedArray[i], imageURL: urlArray?[i] ?? "", type: "image")
                    imageArray.append(model)
                }
                homeVC.sharedCell = imageArray
                homeVC.sharedType = key
            }
            else if key == "video", let sharedArray = userDefaults?.object(forKey: "ImageSharePhotoKey") as? [Data] {
                var imageArray: [VideoCellModel] = []
                
                for i in 0..<sharedArray.count {
                    let urlArray = userDefaults?.object(forKey: "local_path") as? [String]
                    let model = VideoCellModel(imageData: sharedArray[i], imageURL: urlArray?[i] ?? "", type: "video", thumb: "", localPath: urlArray?[i] ?? "")
                    imageArray.append(model)
                    
                }
                homeVC.sharedVideoCell = imageArray
                homeVC.sharedType = key
            }
            else if key == "imageVideo", let sharedArray = userDefaults?.object(forKey: "ImageSharePhotoKey") as? [Data] {
                var imageArray: [VideoCellModel] = []
                
                for i in 0..<sharedArray.count {
                    let urlArray = userDefaults?.object(forKey: "local_path") as? [String]
                    let typeArray = userDefaults?.object(forKey: "imageType") as? [String]
                    let model = VideoCellModel(imageData: sharedArray[i], imageURL: urlArray?[i] ?? "", type: typeArray?[i] ?? "image", thumb: "", localPath: urlArray?[i] ?? "")
                    imageArray.append(model)
                    
                }
                homeVC.sharedVideoCell = imageArray
                homeVC.sharedType = key
            }
                
            else if key == "location" {
                homeVC.sharedType = key
                homeVC.selectedText = userDefaults?.object(forKey: "text") as? String ?? ""
            }
            else if key == "contact" {
                homeVC.sharedType = key
                homeVC.contactData = userDefaults?.object(forKey: "contactData") as! Data
                
            }
            else {
                homeVC.sharedType = key
                homeVC.selectedText = userDefaults?.object(forKey: "text") as? String ?? ""
            }
            self.window?.rootViewController?.present(homeVC, animated: true, completion: nil)
            self.window?.makeKeyAndVisible()
            return true
        }
        else {
            if let key = url.absoluteString.components(separatedBy: "=").last{
                self.window?.rootViewController?.present(homeVC, animated: true, completion: nil)
                self.window?.makeKeyAndVisible()
                return true
                
            }
            
        }
        return false
    }
    
}


@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate, MessagingDelegate {
    
    // MARK:- Messaging Delegates
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
    }
    func callPushNotification() {
        if ( UserModel.shared.getAPNSToken() != nil && UserModel.shared.getPushToken() != nil) {
            if(UserModel.shared.userID() != nil) {
                if  UserModel.shared.isRegistered() != "1" {
                    Utility.shared.registerPushServices()
                }
            }
        }
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("received remote notification \(remoteMessage)")
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)    {
        print(notification)
        let category_type = notification.request.content.categoryIdentifier
        let categoryArr = category_type.components(separatedBy: ":")
        let localObj = LocalStorage()
        let groupObj = groupStorage()
        let channelObj = ChannelStorage()
        
        let badgeNumber = localObj.overAllUnreadMsg() + groupObj.groupOverAllUnreadMsg() + channelObj.channelOverAllUnreadMsg()
        UIApplication.shared.applicationIconBadgeNumber = badgeNumber
        if (UIApplication.shared.keyWindow?.rootViewController as? UITabBarController) != nil {
//            completionHandler([.alert, .badge, .sound])
        } else if let navigationCtrl = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            if categoryArr[0] == "single" {
                if let currentVC = navigationCtrl.visibleViewController as? ChatDetailPage {
                    if currentVC.contact_id != categoryArr[1] {
                        completionHandler([.alert, .badge, .sound])
                    }
                }
                else {
                    completionHandler([.alert, .badge, .sound])
                }
            }
            else if categoryArr[0] == "group"{
                if let currentVC = navigationCtrl.visibleViewController as? GroupChatPage {
                    if currentVC.group_id != categoryArr[1] {
                        completionHandler([.alert, .badge, .sound])
                    }
                }
                else {
                    completionHandler([.alert, .badge, .sound])
                }
            }else if categoryArr[0] == "channel"{
                if let currentVC = navigationCtrl.visibleViewController as? ChannelChatPage {
                    if currentVC.channel_id != categoryArr[1] {
                        completionHandler([.alert, .badge, .sound])
                    }
                }
                else {
                    completionHandler([.alert, .badge, .sound])
                }
            }else if categoryArr[0] == "groupinvitation"{
                completionHandler([.alert, .badge, .sound])
            }else if categoryArr[0] == "channelinvitation"{
                completionHandler([.alert, .badge, .sound])
            }

        } else {
            completionHandler([.alert, .badge, .sound])
        }
        
        // This notification is not auth related, developer should handle it.
        
        //        Auth.auth().canHandleNotification(userInfo)
    }
}

