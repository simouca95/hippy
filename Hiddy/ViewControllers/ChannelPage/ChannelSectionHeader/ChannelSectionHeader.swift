//
//  ChannelSectionHeader.swift
//  Hiddy
//
//  Created by sami hazel on 4/25/20.
//  Copyright © 2020 HITASOFT. All rights reserved.
//

import UIKit

class ChannelSectionHeader: UIView {
    
    @IBOutlet weak var sectionTitle: UILabel!
    @IBOutlet weak var sectionSeeAllButton: UIButton!
    var type = String()
    var parentVC : UIViewController!
    
    
    class func instanceFromNib() -> ChannelSectionHeader {
        let form = UINib(nibName: "ChannelSectionHeader", bundle: nil).instantiate(withOwner: nil, options: nil).first as! ChannelSectionHeader
        form.frame = UIScreen.main.bounds
        return form
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    @IBAction func seeAllAction(_ sender: Any) {
        let myChannelObj = AllChannels()
        myChannelObj.type = self.type
        self.parentVC.navigationController?.pushViewController(myChannelObj, animated: true)
        
    }

    
    func config(sectionTitle : String, vc : UIViewController) {
        
        self.sectionTitle.config(color: .systemBlue, size: 17, align: .center, text: sectionTitle)
        self.type = sectionTitle
        self.parentVC = vc
        if UserModel.shared.getAppLanguage() == "عربى" {
            self.sectionTitle.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.sectionTitle.textAlignment = .right
            self.sectionSeeAllButton.transform = CGAffineTransform(scaleX: -1, y: 1)

        }
        else {
            self.sectionTitle.textAlignment = .right
        }
    }
}
