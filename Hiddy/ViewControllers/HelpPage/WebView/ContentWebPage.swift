//
//  ContentWebPage.swift
//  HSLiveStream
//
//  Created by APPLE on 20/02/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import UIKit
import WebKit

class ContentWebPage: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentWebView: WKWebView!
    
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    var helpDict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.contentWebView.navigationDelegate = self
            self.configureWebviewWithDetails(contentDict: helpDict)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
//        UIApplication.shared.statusBarStyle = .default
        self.changeRTLView()
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }

    func changeRTLView() {
        if UserModel.shared.getAppLanguage() == "عربى" {
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.titleLbl.textAlignment = .right
            self.titleLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.contentWebView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else {
            self.view.transform = .identity
            self.titleLbl.textAlignment = .left
            self.titleLbl.transform = .identity
            self.contentWebView.transform = .identity
        }
    }

    @IBAction func backBtnTapped(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Configure webview load content
    func configureWebviewWithDetails(contentDict:NSDictionary)  {
        self.navigationView.elevationEffect()
        self.titleLbl.config(color: TEXT_PRIMARY_COLOR, size: 20, align: .left, text:EMPTY_STRING)
        self.titleLbl.text = contentDict.value(forKey: "title") as? String
    
        let contentString:String = contentDict.value(forKey: "description") as! String
        let htmlString:String = "<font face=\(APP_FONT_REGULAR) size='10'>\(contentString)"
        self.contentWebView.loadHTMLString(htmlString, baseURL: nil)
        self.indicator.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.indicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.indicator.stopAnimating()
    }
}
