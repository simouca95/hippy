//
//  BaseTableViewCell.swift
//  Hiddy
//
//  Created by sami hazel on 5/1/20.
//  Copyright © 2020 HITASOFT. All rights reserved.
//

import UIKit
import Reactions

class BaseTableViewCell: UITableViewCell {

    var reactionsView: ReactionView!
    

    let select       = ReactionSelector()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.reactionsView = ReactionView(frame: CGRect(x: 0, y: 0, width: self.contentView.frame.width, height: 50))
        self.setReactionsView()
        
        self.contentView.addSubview(reactionsView)
    }
    
    func presentReactionSelector() {
        self.reactionsView.reactionButton.presentReactionSelector()
    }
    
    func setReactionsView() {
        select.reactions = Reaction.hiddy.all
        select.config = ReactionSelectorConfig {
            $0.spacing        = 3
            $0.iconSize       = 30
            $0.stickyReaction = true
        }
        
        self.reactionsView.reactionButton.reaction = Reaction.hiddy.like
        self.reactionsView.reactionButton.reactionSelector = select
        self.reactionsView.reactionButton.addTarget(self, action: #selector(facebookButtonReactionTouchedUpAction), for: .touchUpInside)
        
        let summary = ReactionSummary()
        summary.reactions = Reaction.hiddy.all
        summary.setDefaultText(withTotalNumberOfPeople: 6, includingYou: true)
        summary.config = ReactionSummaryConfig {
            $0.spacing      = 0
            $0.iconMarging  = 1
            $0.font         = UIFont(name: "HelveticaNeue", size: 12)
            $0.textColor    = UIColor(red: 0.47, green: 0.47, blue: 0.47, alpha: 1)
            $0.alignment    = .left
            $0.isAggregated = true
        }
        self.reactionsView.summaryView = summary
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @objc func facebookButtonReactionTouchedUpAction(_ sender: AnyObject) {
        if self.reactionsView.reactionButton.isSelected == false {
            self.reactionsView.reactionButton.reaction   = Reaction.facebook.like
      }
    }
}
