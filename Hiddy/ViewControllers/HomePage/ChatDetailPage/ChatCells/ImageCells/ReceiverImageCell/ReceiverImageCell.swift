//
//  ReceiverImageCell.swift
//  Hiddy
//
//  Created by APPLE on 06/06/18.
//  Copyright © 2018 HITASOFT. All rights reserved.
//

import UIKit
import Lottie

class ReceiverImageCell: UITableViewCell {

    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var containerView: UIView!
    @IBOutlet var ImageFileView: UIImageView!
    @IBOutlet var downloadIcon: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var loader: AnimationView!
    @IBOutlet var downloadView: UIView!
    
    @IBOutlet weak var imageBtn: UIButton!
    
    var blurView = UIVisualEffectView()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.specificCornerRadius(radius: 15)
        timeLbl.config(color: .white, size: 14, align: .left, text: EMPTY_STRING)
        self.ImageFileView.layer.minificationFilter = CALayerContentsFilter.linear
        self.nameLbl.config(color: TEXT_PRIMARY_COLOR, size: 16, align: .left, text: EMPTY_STRING)
        loader = AnimationView.init(name: "Download")
        loader.frame = CGRect.init(x: (self.containerView.frame.size.width/2)-20, y: (self.containerView.frame.size.height/2)-20, width: 40, height: 40)
        loader.loopMode = .loop
//        loader.loopAnimation = true
        loader.animationSpeed = 2
        self.containerView.addSubview(loader)
        self.downloadView.cornerViewRadius()
        if UserModel.shared.getAppLanguage() == "عربى" {
            self.timeLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.timeLbl.textAlignment = .left
            self.nameLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.nameLbl.textAlignment = .right
            self.ImageFileView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        else {
            self.timeLbl.transform = .identity
            self.timeLbl.textAlignment = .right
            self.nameLbl.transform = .identity
            self.nameLbl.textAlignment = .left
            self.ImageFileView.transform = .identity
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func config(msgDict:NSDictionary)  {
        self.nameLbl.isHidden = true
        let imageName:String = msgDict.value(forKeyPath: "message_data.attachment") as! String
        // Print("Image URL \(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(imageName)")
        let time:String = msgDict.value(forKeyPath: "message_data.chat_time") as! String
        self.timeLbl.text = Utility.shared.chatTime(stamp: Utility.shared.convertToDouble(string: time))
        let isDownload:String = msgDict.value(forKeyPath: "message_data.isDownload") as! String
        if isDownload == "0" {
            self.ImageFileView.layer.minificationFilter = CALayerContentsFilter.trilinear
            self.ImageFileView.layer.minificationFilterBias = 3.0
            self.downloadIcon.isHidden = false
            self.downloadView.isHidden = false
            self.loader.isHidden = true
        }else if isDownload == "1"{
            self.ImageFileView.layer.minificationFilterBias = 0.0
            self.ImageFileView.sd_setImage(with: URL(string:"\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(imageName)"), placeholderImage: #imageLiteral(resourceName: "profile_placeholder"))
            self.downloadIcon.isHidden = true
            self.downloadView.isHidden = true
            self.loader.stop()
            self.loader.isHidden = true
        }else if isDownload == "2"{
            self.ImageFileView.layer.minificationFilter = CALayerContentsFilter.trilinear
            self.ImageFileView.layer.minificationFilterBias = 3.0
            self.downloadIcon.isHidden = true
            self.downloadView.isHidden = true
            self.loader.isHidden = false
            self.loader.play()
        }
        self.loadImage(url: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(imageName)")
    }
    func loadImage(url: String) {
        DispatchQueue.main.async {
            self.ImageFileView.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "profile_placeholder"))
        }
    }

    func configGroupMsg(model:groupMsgModel.message)  {
        DispatchQueue.main.async {
            self.timeLbl.config(color: .white, size: 14, align: .right, text: EMPTY_STRING)
            self.nameLbl.isHidden = false
            self.nameLbl.text = Utility.shared.getUsername(user_id: model.member_id)
            self.nameLbl.frame = CGRect.init(x: 10, y: 0, width: self.containerView.frame.width, height: 20)
            self.containerView.frame = CGRect.init(x: 10, y: 22, width: 210, height: 140)
            self.timeLbl.frame = CGRect.init(x: 10, y: 110, width: 210, height: 20)
            self.imageBtn.frame = CGRect.init(x: 10, y: 22, width: 210, height: 140)
            self.addSubview(self.imageBtn)
            self.addSubview(self.nameLbl)
            self.addSubview(self.containerView)
            self.addSubview(self.timeLbl)
            self.bringSubviewToFront(self.imageBtn)
        }
        self.timeLbl.text = Utility.shared.chatTime(stamp: Utility.shared.convertToDouble(string: model.timestamp))
        self.loadImage(url: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(model.attachment)")
        if model.isDownload == "0" {
            self.ImageFileView.layer.minificationFilter = CALayerContentsFilter.trilinear
            self.ImageFileView.layer.minificationFilterBias = 3.0
            self.downloadIcon.isHidden = false
            self.downloadView.isHidden = false
            self.loader.isHidden = true
        }else if model.isDownload == "1"{
            self.ImageFileView.layer.minificationFilterBias = 0.0
            self.ImageFileView.sd_setImage(with: URL(string:"\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(model.attachment)"), placeholderImage: #imageLiteral(resourceName: "profile_placeholder"))
            self.downloadIcon.isHidden = true
            self.downloadView.isHidden = true
            self.loader.stop()
            self.loader.isHidden = true
        }else if model.isDownload == "2"{
            self.ImageFileView.layer.minificationFilter = CALayerContentsFilter.trilinear
            self.ImageFileView.layer.minificationFilterBias = 3.0
            self.downloadIcon.isHidden = true
            self.downloadView.isHidden = true
            self.loader.isHidden = false
            self.loader.play()
        }
    }
    
//    channel msg
    func configChannelMsg(model:channelMsgModel.message)  {
        self.nameLbl.isHidden = true
        self.loadImage(url: "\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(model.attachment)")
        self.timeLbl.text = Utility.shared.chatTime(stamp: Utility.shared.convertToDouble(string: model.timestamp))
        if model.isDownload == "0" {
            self.ImageFileView.layer.minificationFilter = CALayerContentsFilter.trilinear
            self.ImageFileView.layer.minificationFilterBias = 3.0
            self.downloadIcon.isHidden = false
            self.downloadView.isHidden = false
            self.loader.isHidden = true
        }else if model.isDownload == "1"{
            self.ImageFileView.layer.minificationFilterBias = 0.0
            self.ImageFileView.sd_setImage(with: URL(string:"\(IMAGE_BASE_URL)\(IMAGE_SUB_URL)\(model.attachment)"), placeholderImage: #imageLiteral(resourceName: "profile_placeholder"))
            self.downloadIcon.isHidden = true
            self.downloadView.isHidden = true
            self.loader.stop()
            self.loader.isHidden = true
        }else if model.isDownload == "2"{
            self.ImageFileView.layer.minificationFilter = CALayerContentsFilter.trilinear
            self.ImageFileView.layer.minificationFilterBias = 3.0
            self.downloadIcon.isHidden = true
            self.downloadView.isHidden = true
            self.loader.isHidden = false
            self.loader.play()
        }
    }
}
