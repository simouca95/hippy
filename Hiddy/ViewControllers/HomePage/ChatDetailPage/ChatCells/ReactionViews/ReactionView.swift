//
//  ReactionView.swift
//  Hiddy
//
//  Created by sami hazel on 5/1/20.
//  Copyright © 2020 HITASOFT. All rights reserved.
//

import UIKit
import Reactions

class ReactionView: UIView {

    @IBOutlet weak var reactionButton: ReactionButton!
    @IBOutlet weak var summaryView: ReactionSummary!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
