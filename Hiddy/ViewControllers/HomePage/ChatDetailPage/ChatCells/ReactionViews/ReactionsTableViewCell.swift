//
//  ReactionsTableViewCell.swift
//  Hiddy
//
//  Created by sami hazel on 5/1/20.
//  Copyright © 2020 HITASOFT. All rights reserved.
//

import UIKit
import Reactions

class ReactionsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var reactionButton: ReactionButton!
    @IBOutlet weak var summaryView: ReactionSummary!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
