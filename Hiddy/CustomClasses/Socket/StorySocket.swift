//
//  StorySocket.swift
//  Hiddy
//
//  Created by Hitasoft on 05/08/19.
//  Copyright © 2019 HITASOFT. All rights reserved.
//

import Foundation
protocol storyDelegate {
    func gotStoryInfo(dict:NSArray,type:String)
}
class StorySocket  {
    static let sharedInstance = StorySocket()
    var delegate : storyDelegate?
    let groupDB = storyStorage()
    let localDB = LocalStorage()
    
    //send request to story Viewer
    func storyView(sender_id:String,receiver_id:String, story_id: String) {
        let requestDict = NSMutableDictionary()
        requestDict.setValue(sender_id, forKey: "sender_id")
        requestDict.setValue(receiver_id, forKey: "receiver_id")
        requestDict.setValue(story_id, forKey: "story_id")
        socket.defaultSocket.emit("viewstory", requestDict)
    }
    func deleteStory(story_id: String, memberID: String) {
        let requestDict = NSMutableDictionary()
        let StrMembers = memberID.components(separatedBy: ",")
        let memberArr = getMemberId(selectedId: StrMembers)
        requestDict.setValue(story_id, forKey: "story_id")
        requestDict.setValue(memberArr, forKey: "story_members")
        print(requestDict)
        socket.defaultSocket.emit("deletestory", requestDict)
    }
    func getMemberId(selectedId: Array<Any>)->NSMutableArray{
        let mutableArray = NSMutableArray()
        for id in selectedId {
            let dict = NSMutableDictionary()
            dict.setValue(id, forKey: "member_id")
            dict.setValue("0", forKey: "member_role")
            let memberDict = ["member_id":id]
            mutableArray.add(memberDict)
        }
        return mutableArray
    }
    func postStory(user_id: String, story_id: String, stories: NSDictionary) {
        let requestDict = stories
        requestDict.setValue("ios", forKey: "device_type")
//        requestDict.setValue(UserModel.shared.userID(), forKey: "user_id")
//        requestDict.setValue(story_id, forKey: "story_id")
//        requestDict.setValue(stories, forKey: "stories")
        socket.defaultSocket.emit("poststory", stories)
        // Print(requestDict)
//        (user_id,story_id,sender_id,story_members,message,story_type,attachment,story_date,story_time,expiry_time)
    }
    func updateReceivedSocket(story_id: String) {
        let requestDict = NSMutableDictionary()
        requestDict.setValue(story_id, forKey: "story_id")
        requestDict.setValue(UserModel.shared.userID() as String? ?? "", forKey: "user_id")
        socket.defaultSocket.emit("storyofflineclear", requestDict)
    }
//    func updateDeletedStory(story_id: String) {
//        let requestDict = NSMutableDictionary()
//        requestDict.setValue(story_id, forKey: "story_id")
//        requestDict.setValue(UserModel.shared.userID() as String? ?? "", forKey: "user_id")
//        socket.defaultSocket.emit("storyofflinedelete", requestDict)
//    }
    func addStoryHandler() {
        socket.defaultSocket.on("receivestory") { (data, ack) in
            // Print("SOCKET NEW receive Status MESSAGE \(data)")
            let value = data as NSArray
            
            self.delegate?.gotStoryInfo(dict: value, type: "receivestory")
        }
        socket.defaultSocket.on("getbackstatus") { ( data, ack) in
            // Print("SOCKET NEW getBack Status Status MESSAGE \(data)")
            let value = data as NSArray
            let storyDict = value[0] as? NSDictionary ?? ["stories":""]
            self.delegate?.gotStoryInfo(dict: storyDict["stories"] as? NSArray ?? [""], type: "getbackstatus")
        }
        socket.defaultSocket.on("storyviewed") { ( data, ack) in
            // Print("SOCKET NEW Status MESSAGE \(data)")
            let value = data as NSArray
            let storyDict = value[0] as? NSDictionary ?? ["viewers":""]
            self.delegate?.gotStoryInfo(dict: storyDict["viewers"] as? NSArray ?? [""], type: "storyviewed")
        }
        socket.defaultSocket.on("storydeleted") { ( data, ack) in
            // Print("SOCKET NEW Status MESSAGE \(data)")
            let value = data as NSArray
            print(value)
            self.delegate?.gotStoryInfo(dict: value, type: "stroydeleted")
        }
        socket.defaultSocket.on("storyofflinedelete") { ( data, ack) in
            // Print("SOCKET NEW Status MESSAGE \(data)")
            let value = data as NSArray
            self.delegate?.gotStoryInfo(dict: value, type: "storyofflinedelete")
        }
    }
}
