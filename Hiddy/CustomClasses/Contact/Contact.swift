//
//  Contact.swift
//  Hiddy
//
//  Created by APPLE on 03/07/18.
//  Copyright © 2018 HITASOFT. All rights reserved.
//

import Foundation
import Contacts
import PhoneNumberKit

class Contact{
    static let sharedInstance = Contact()
  
    let contactStore = CNContactStore()
    let phoneNoArray = NSMutableArray()
    let phoneNumberKit = PhoneNumberKit()
    let phoneContacts = NSMutableArray()

    //get all contact list
    func synchronize() {
        self.phoneNoArray.removeAllObjects()
        self.phoneContacts.removeAllObjects()
        UserModel.shared.removeContactList()
        
        let keys = [CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactNicknameKey]
        
        let request1 = CNContactFetchRequest(keysToFetch: keys  as [CNKeyDescriptor])
        try? contactStore.enumerateContacts(with: request1) { (contact, error) in
            for people in contact.phoneNumbers {
                // Whatever you want to do with it
                do {
                    let phoneNumber = try self.phoneNumberKit.parse(people.value.stringValue, withRegion: "GB", ignoreType: true)
                    self.phoneNoArray.add(phoneNumber.nationalNumber)
                    let dict = ["phone_no":"\(phoneNumber.nationalNumber)","contact_name":"\(contact.givenName)"]
                    self.phoneContacts.add(dict)
                }
                catch {
                    
                }
            }
        }
        
        UserModel.shared.setAllContacts(contacts: self.phoneContacts as! [[String : String]])

//        if ((self.phoneContacts as? [String : String]) != nil) {
//            UserModel.shared.setAllContacts(contacts: self.phoneContacts as! [[String : String]])
//        }
        // save my conatcts
        let userObj = UserWebService()
        // Print("conatct array \(self.phoneNoArray.count)")
        userObj.saveContacts(contacts: self.phoneNoArray, onSuccess: {response in
            let status:String = response.value(forKey: "status") as! String
            if status == STATUS_TRUE{
                let localDB = LocalStorage()
                let localArray = localDB.getLocalPhoneNumbers()
                self.phoneNoArray.addObjects(from: localArray as! [Any])
                self.updateMyContacts()
            }
        })
    }
    func updateBadgeStatus() {
        
    }
    //update my contacts
    func updateMyContacts()  {
        if self.phoneNoArray.count != 0{
        let userObj = UserWebService()
        userObj.setContacts(contacts: self.phoneNoArray, onSuccess: {response in
            let status:String = response.value(forKey: "status") as! String
            if status == STATUS_TRUE{
                let tempArray = NSMutableArray()
                tempArray.addObjects(from: (response.value(forKey: "result") as! NSArray) as! [Any])
                
                self.addToDB(contact: tempArray)
            }
        })
        }
        
    }
    
    //add contat to local db
    func addToDB(contact:NSMutableArray)  {
        let localObj = LocalStorage()
        for contactDict in contact {
            let userDict:NSDictionary = contactDict as! NSDictionary
            let phoneNo:NSNumber = userDict.value(forKey: "phone_no") as! NSNumber
            let cc = userDict.value(forKey: "country_code") as! Int

            DispatchQueue.global(qos: .background).async {
                var name = String()
                let contactName = Utility.shared.searchPhoneNoAvailability(phoneNo: "\(phoneNo)")
                if contactName == EMPTY_STRING{
                    name = "+\(cc) " + "\(phoneNo)"
                }else{
                    name = contactName
                }
        
                DispatchQueue.main.async {
                    let type = String()
                    var username = String()
                    if userDict.value(forKey: "user_name") != nil{
                        username = userDict.value(forKey: "user_name") as! String
                    }else {
                        username = EMPTY_STRING
                    }
                    localObj.addContact(userid: userDict.value(forKey: "_id") as! String,
                                        contactName: name,
                                        userName:username,
                                        phone:String(describing: phoneNo) ,
                                        img: userDict.value(forKey: "user_image") as! String,
                                        about: userDict.value(forKey: "about") as! String,
                                        type:type,
                                        mutual:userDict.value(forKey: "contactstatus") as! String,
                                        privacy_lastseen: userDict.value(forKey: "privacy_last_seen") as! String,
                                        privacy_about: userDict.value(forKey: "privacy_about") as! String,
                                        privacy_picture: userDict.value(forKey: "privacy_profile_image") as! String, countryCode: String(cc))
                }
            }
        }
    }
}
