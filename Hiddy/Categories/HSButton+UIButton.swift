//
//  HSButton+UIButton.swift
//  HSTaxiUserApp
//
//  Created by APPLE on 10/03/18.
//  Copyright © 2018 APPLE. All rights reserved.
//

import Foundation
import UIKit
import Reactions
extension UIButton{
    public func config(color:UIColor,size:CGFloat,align:UIControl.ContentHorizontalAlignment,title:String){
        self.setTitleColor(color, for: .normal)
        self.setTitle(Utility().getLanguage()?.value(forKey: title) as? String, for: .normal)
        self.contentHorizontalAlignment = align
        self.titleLabel?.font = UIFont.init(name:APP_FONT_REGULAR, size: size)
    }
    func cornerRoundRadius() {
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
    }
    func cornerMiniumRadius() {
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
    func setBorder(color:UIColor) {
        self.layer.borderWidth = 1
        self.layer.borderColor = color.cgColor
    }
    
    //MARK: shadow effect
    func floatingEffect() {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.layer.shadowRadius = 3;
        self.layer.shadowOpacity = 0.5;
    }
  
   func newEffect() {
        self.backgroundColor = .white
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize.init(width: 0.0, height: 2)
        self.layer.shadowRadius = 1.0;
        self.layer.shadowOpacity = 1;
    
    }
}
extension Reaction {
  /// Struct which defines the standard facebook reactions.
  public struct hiddy {
    /// The facebook's "like" reaction.
    public static var like: Reaction {
      return reactionWithId("like-reaction")
    }
    public static var pray: Reaction {
      return reactionWithId("pray-reaction")
    }
    public static var dislike: Reaction {
      return reactionWithId("dislike-reaction")
    }


    /// The list of standard facebook reactions in this order: `.like`, `.love`, `.haha`, `.wow`, `.sad`, `.angry`.
    public static let all: [Reaction] = [facebook.like , hiddy.dislike , hiddy.pray ,facebook.love, facebook.haha, facebook.wow, facebook.sad, facebook.angry]

    // MARK: - Convenience Methods

    private static func reactionWithId(_ id: String) -> Reaction {
      var color: UIColor            = .black
      var alternativeIcon: UIImage? = nil

      switch id {
      case "like-reaction":
        color           = UIColor(red: 0.29, green: 0.54, blue: 0.95, alpha: 1)
        alternativeIcon = imageWithName("like-reaction").withRenderingMode(.alwaysTemplate)
      default:
        color = UIColor(red: 0.99, green: 0.84, blue: 0.38, alpha: 1)
      }

      return Reaction(id: id, title: id, color: color, icon: imageWithName(id), alternativeIcon: alternativeIcon)
    }

    private static func imageWithName(_ name: String) -> UIImage {
        return UIImage(named: name)!
    }
  }
}

